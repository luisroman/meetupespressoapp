package com.mytaxi.mycoins

import android.os.Bundle
import android.support.constraint.Guideline
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.mytaxi.mycoins.models.info.Urls
import com.mytaxi.mycoins.models.info.X10
import com.mytaxi.mycoins.retrofit.RetrofitBuilder
import com.mytaxi.mycoins.service.CryptocurrencyService
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MoreInfoActivity : AppCompatActivity() {

    @BindView(R.id.moreSymbol)
    lateinit var moreSymbol: TextView

    @BindView(R.id.moreName)
    lateinit var moreName: TextView

    @BindView(R.id.moreGuideline)
    lateinit var moreGuideline: Guideline

    @BindView(R.id.multiText)
    lateinit var multiText: TextView

    private var id: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_more_info)
        ButterKnife.bind(this)
        val bundle = intent.extras
        id = bundle!!.getInt("id")
        val cryptocurrencyService: CryptocurrencyService = RetrofitBuilder.retrofitBuilder.create(CryptocurrencyService::class.java)
        val callInfo: Call<JsonObject> = cryptocurrencyService.infoCall(id)
        callInfo.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                if (response.code() == 200) {
                    val jsonObject = JSONObject(response.body().toString())
                    val jsonObjectData: JSONObject = jsonObject.getJSONObject("data")
                    val x10: X10 = Gson().fromJson(jsonObjectData.getJSONObject(id.toString()).toString(), X10::class.java)
                    moreSymbol.text = x10.symbol
                    moreName.text = x10.name
                    val urls: Urls = x10.urls
                    for (x in 0 until urls.website.size) {
                        multiText.append(urls.website[x].toString() + "\n\n")
                    }
                    for (x in 0 until urls.twitter.size) {
                        multiText.append(urls.twitter[x].toString() + "\n\n")
                    }
                    for (x in 0 until urls.message_board.size) {
                        multiText.append(urls.message_board[x].toString() + "\n\n")
                    }
                    for (x in 0 until urls.source_code.size) {
                        multiText.append(urls.source_code[x].toString() + "\n\n")
                    }
                    for (x in 0 until urls.explorer.size) {
                        multiText.append(urls.explorer[x].toString() + "\n\n")
                    }
                } else {
                    Log.d("debug", response.message())
                    Toast.makeText(applicationContext, R.string.toast_message, Toast.LENGTH_SHORT).show()
                }

            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Log.d("debug", t.toString())
                Toast.makeText(applicationContext, R.string.toast_message, Toast.LENGTH_SHORT).show()
            }
        })
    }
}
