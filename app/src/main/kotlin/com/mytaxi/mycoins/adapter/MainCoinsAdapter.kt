package com.mytaxi.mycoins.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.mytaxi.mycoins.R

class MainCoinsAdapter(private val coinsList: List<MainCoins>) : RecyclerView.Adapter<MainCoinsAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var title: TextView
        var symbol: TextView

        init {
            title = view.findViewById<View>(R.id.title) as TextView
            symbol = view.findViewById<View>(R.id.symbol) as TextView
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.coins_type_list, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val mainCoins = coinsList[position]
        holder.title.text = mainCoins.title
        holder.symbol.text = mainCoins.symbol
    }

    override fun getItemCount(): Int {
        return coinsList.size
    }


}