package com.mytaxi.mycoins

import android.content.Intent
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.RootMatchers.withDecorView
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.test.runner.AndroidJUnit4
import com.mytaxi.mycoins.mock.Dispatchers
import org.hamcrest.CoreMatchers.not
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class ListingActivityKoTest : BaseTest<ListingActivity>() {

    override fun getTestActivity() = IntentsTestRule(ListingActivity::class.java, true, false)

    @Before
    override fun setUp() {
        super.setUp()
        val intent = Intent()
        intent.putExtra("id", 1)
        activityTestRule.launchActivity(intent)
    }

    @Test
    fun testToastMessageInListingError() {
        onView(withText(R.string.toast_message)).inRoot(withDecorView(not(activityTestRule.getActivity().getWindow().getDecorView()))).check(matches(isDisplayed()))
    }

    companion object {

        @JvmStatic
        @BeforeClass
        fun setUpClass() {
            setDispatcher(Dispatchers.koDispatcher)
        }

    }

}