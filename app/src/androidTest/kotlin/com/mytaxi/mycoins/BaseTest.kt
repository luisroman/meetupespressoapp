package com.mytaxi.mycoins

import android.app.Activity
import android.support.test.espresso.IdlingRegistry
import android.support.test.espresso.intent.rule.IntentsTestRule
import com.mytaxi.mycoins.retrofit.RetrofitBuilder
import com.jakewharton.espresso.OkHttp3IdlingResource
import android.support.test.espresso.IdlingResource
import com.mytaxi.mycoins.custom.UrlDispatcher
import com.mytaxi.mycoins.mock.Dispatchers
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockWebServer
import org.junit.*


abstract class BaseTest<T: Activity> {

    abstract fun getTestActivity(): IntentsTestRule<T>

    @Rule
    @JvmField
    var activityTestRule = getTestActivity()

    @Before
    open fun setUp() {
        IdlingRegistry.getInstance().register(idlingResource)
    }

    @After
    fun tearDown() {
        activityTestRule.finishActivity()
        IdlingRegistry.getInstance().unregister(idlingResource)
    }

    companion object {
        private var server: MockWebServer? = null

        private val idlingResource = OkHttp3IdlingResource.create("apptest", RetrofitBuilder.okHttpClient)

        private fun setUpServerAddress() {
            UrlDispatcher.mockedUrl = server?.url("/").toString()
        }

        fun setDispatcher(dispatcher: Dispatcher) {
            server = MockWebServer()
            server?.setDispatcher(dispatcher)
            setUpServerAddress()
        }

        @JvmStatic
        @AfterClass
        fun tearDownClass() {
            server?.close()
            server?.shutdown()
        }
    }

}