package com.mytaxi.mycoins

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.Intents.intended
import android.support.test.espresso.intent.matcher.IntentMatchers.*
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.runner.AndroidJUnit4
import com.mytaxi.mycoins.helpers.MyViewAction
import com.mytaxi.mycoins.mock.Dispatchers
import org.hamcrest.CoreMatchers.allOf
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class MainActivityTest : BaseTest<MainActivity>() {

    override fun getTestActivity() = IntentsTestRule(MainActivity::class.java, true, false)

    @Before
    override fun setUp() {
        super.setUp()
        activityTestRule.launchActivity(null)
    }

    @Test
    fun testRecyclerViewHasBeenLoaded() {
        onView(MyViewAction.withIndex(withId(R.id.mainRelativeLayout),0)).check(matches(isDisplayed()))
    }

    @Test
    fun testClickRelativeLayoutAndCheckIntendedData() {
        onView(MyViewAction.withIndex(withId(R.id.mainRelativeLayout),0)).perform(click())
        intended(allOf(hasComponent(ListingActivity::class.java.name), hasExtra("id", 1)))
    }

    companion object {

        @JvmStatic
        @BeforeClass
        fun setUpClass() {
            setDispatcher(Dispatchers.okDispatcher)
        }

    }

}
