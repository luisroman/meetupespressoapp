package com.mytaxi.mycoins

import android.content.Intent
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.Intents.intended
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.runner.AndroidJUnit4
import com.mytaxi.mycoins.mock.Dispatchers
import org.hamcrest.CoreMatchers
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ListingActivityTest : BaseTest<ListingActivity>() {

    override fun getTestActivity() = IntentsTestRule(ListingActivity::class.java, true, false)

    @Before
    override fun setUp() {
        super.setUp()
        val intent = Intent()
        intent.putExtra("id", 1)
        activityTestRule.launchActivity(intent)
    }

    @Test
    fun testMoreInfoScreenHasBeenLoaded() {
        onView(withId(R.id.moreInfoButton)).check(matches(isDisplayed()))
    }

    @Test
    fun testClickMoreInfoScreenAndCheckIntendedData() {
        onView(withId(R.id.moreInfoButton)).perform(click())
        intended(CoreMatchers.allOf(hasComponent(MoreInfoActivity::class.java.name), hasExtra("id", 1)))
    }

    companion object {

        @JvmStatic
        @BeforeClass
        fun setUpClass() {
            setDispatcher(Dispatchers.okDispatcher)
        }

    }
}
