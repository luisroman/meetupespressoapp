package com.mytaxi.mycoins.service

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.mytaxi.mycoins.models.listings.Listings
import com.mytaxi.mycoins.models.map.Map
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface CryptocurrencyService {

    @Headers(apiKey)
    @GET("v1/cryptocurrency/map?limit=100")
    fun mapCall(): Call<Map>


    @Headers(apiKey)
    @GET("v1/cryptocurrency/listings/latest?limit=5000")
    fun listingCall(): Call<Listings>

    @Headers(apiKey)
    @GET("v1/cryptocurrency/info")
    fun infoCall(@Query("id") id: Int): Call<JsonObject>


    companion object {
        private const val apiKey  = "X-CMC_PRO_API_KEY: d5c34b5f-a9a1-41bb-8376-a047845aefa9"
    }
}