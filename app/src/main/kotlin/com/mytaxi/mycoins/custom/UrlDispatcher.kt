package com.mytaxi.mycoins.custom

import android.app.Application
import android.content.Context

object UrlDispatcher {

    var mockedUrl: String? = null

    fun url(): String? {
        return if (mockedUrl != null) {
            mockedUrl
        } else {
            "https://pro-api.coinmarketcap.com/"
        }
    }

}