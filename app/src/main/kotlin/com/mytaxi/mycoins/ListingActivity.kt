package com.mytaxi.mycoins

import android.content.Intent
import android.os.Bundle
import android.support.constraint.Guideline
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.mytaxi.mycoins.models.listings.Data
import com.mytaxi.mycoins.models.listings.Listings
import com.mytaxi.mycoins.models.listings.Quote
import com.mytaxi.mycoins.models.listings.USD
import com.mytaxi.mycoins.retrofit.RetrofitBuilder
import com.mytaxi.mycoins.service.CryptocurrencyService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ListingActivity : AppCompatActivity() {

    @BindView(R.id.listingSymbol)
    lateinit var listingSymbol: TextView

    @BindView(R.id.listingName)
    lateinit var listingName: TextView

    @BindView(R.id.listingPrice)
    lateinit var listingPrice: TextView

    @BindView(R.id.listingChangeOneHr)
    lateinit var listingOneHr: TextView

    @BindView(R.id.listingChangeOneDay)
    lateinit var listingOneDay: TextView

    @BindView(R.id.listingChangeSevenDay)
    lateinit var listingSevenDay: TextView

    @BindView(R.id.guideline)
    lateinit var guideline: Guideline

    @BindView(R.id.moreInfoButton)
    lateinit var moreInfoButton: Button

    private var jsonElements: List<Data>? = null

    private var id: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listing)
        ButterKnife.bind(this)
        val bundle = intent.extras
        id = bundle!!.getInt("id")
        val cryptocurrencyService: CryptocurrencyService = RetrofitBuilder.retrofitBuilder.create(CryptocurrencyService::class.java)
        val callListings: Call<Listings> = cryptocurrencyService.listingCall()
        callListings.enqueue(object : Callback<Listings> {
            override fun onResponse(call: Call<Listings>, response: Response<Listings>) {
                if (response.code() == 200) {
                    val listingsObject: Listings? = response.body()
                    jsonElements = listingsObject?.data
                    fillData()
                } else {
                    Log.d("debug", response.message())
                    Toast.makeText(applicationContext, R.string.toast_message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<Listings>, t: Throwable) {
                Log.d("debug", t.toString())
                Toast.makeText(applicationContext, "There's a problem with the server, try again later", Toast.LENGTH_SHORT).show()
            }
        })

    }

    fun fillData() {
        for (x in 0 until jsonElements!!.size) {
            if (jsonElements!![x].id == id) {
                listingSymbol.text = jsonElements!![x].symbol
                listingName.text = jsonElements!![x].name
                val quote: Quote = jsonElements!![x].quote
                val usd: USD = quote.USD
                listingPrice.text = "Price (USD): " + usd.price
                listingOneHr.text = "Change % 1h: " + usd.percent_change_1h
                listingOneDay.text = "Change % 1day: " + usd.percent_change_24h
                listingSevenDay.text = "Change % 7days: " + usd.percent_change_7d
                moreInfoButton.visibility = View.VISIBLE
                break
            }
        }
    }

    @OnClick(R.id.moreInfoButton)
    fun openMoreInfo() {
        val intent = Intent(this, MoreInfoActivity::class.java)
        intent.putExtra("id", id)
        startActivity(intent)
    }


}
