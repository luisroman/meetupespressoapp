package com.mytaxi.mycoins.retrofit

import com.mytaxi.mycoins.custom.UrlDispatcher
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {

    var okHttpClient = OkHttpClient()

    val retrofitBuilder: Retrofit
        get() {
            return Retrofit.Builder().baseUrl(UrlDispatcher.url()!!).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build()
        }

}