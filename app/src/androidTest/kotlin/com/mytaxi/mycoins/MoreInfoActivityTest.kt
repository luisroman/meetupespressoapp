package com.mytaxi.mycoins

import android.content.Intent
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.Intents.intended
import android.support.test.espresso.intent.matcher.IntentMatchers
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.runner.AndroidJUnit4
import com.mytaxi.mycoins.mock.Dispatchers
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.containsString
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MoreInfoActivityTest : BaseTest<MoreInfoActivity>() {

    override fun getTestActivity() = IntentsTestRule(MoreInfoActivity::class.java, true, false)

    @Before
    override fun setUp() {
        super.setUp()
        val intent = Intent()
        intent.putExtra("id", 1)
        activityTestRule.launchActivity(intent)
    }

    @Test
    fun testMoreInfoLinksHasBeenLoaded() {
        onView(withId(R.id.multiText)).check(matches(withText(containsString("mytaxi"))))
    }

    companion object {

        @JvmStatic
        @BeforeClass
        fun setUpClass() {
            setDispatcher(Dispatchers.okDispatcher)
        }

    }
}
