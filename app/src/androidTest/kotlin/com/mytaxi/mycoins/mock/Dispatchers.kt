package com.mytaxi.mycoins.mock

import android.support.test.InstrumentationRegistry
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest
import org.apache.commons.io.IOUtils
import java.io.IOException
import java.io.InputStream

object Dispatchers {

    internal val okDispatcher: Dispatcher = object : Dispatcher() {
        @Throws(InterruptedException::class)
        override fun dispatch(request: RecordedRequest): MockResponse {
            return if (request.path.contains("v1/cryptocurrency/map")) {
                MockResponse().setResponseCode(200).setBody(String(readJson("json/map.json")!!))
            } else if (request.path.contains("v1/cryptocurrency/info")) {
                MockResponse().setResponseCode(200).setBody(String(readJson("json/info.json")!!))
            } else if (request.path.contains("v1/cryptocurrency/listings/latest")) {
                MockResponse().setResponseCode(200).setBody(String(readJson("json/listings.json")!!))
            } else {
                MockResponse().setResponseCode(404)
            }
        }
    }

    internal val koDispatcher: Dispatcher = object : Dispatcher() {
        @Throws(InterruptedException::class)
        override fun dispatch(request: RecordedRequest): MockResponse {
            return if (request.path.contains("v1/cryptocurrency/map")) {
                MockResponse().setResponseCode(500)
            } else if (request.path.contains("v1/cryptocurrency/info")) {
                MockResponse().setResponseCode(500)
            } else if (request.path.contains("v1/cryptocurrency/listings/latest")) {
                MockResponse().setResponseCode(500)
            } else {
                MockResponse().setResponseCode(404)
            }
        }
    }



    private fun readJson(path: String): ByteArray? {
        val jsonFile: InputStream?
        var jsonBytes: ByteArray? = null
        try {
            jsonFile = InstrumentationRegistry.getContext().assets.open(path)
            jsonBytes = IOUtils.toByteArray(jsonFile!!)
        } catch (exception: IOException) {
            exception.printStackTrace()
        }
        return jsonBytes
    }

}