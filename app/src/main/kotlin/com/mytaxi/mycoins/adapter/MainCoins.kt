package com.mytaxi.mycoins.adapter

class MainCoins {
    var title: String? = null
    var symbol: String? = null

    constructor() {}

    constructor(title: String, symbol: String) {
        this.title = title
        this.symbol = symbol
    }
}