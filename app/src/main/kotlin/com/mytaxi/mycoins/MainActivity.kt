package com.mytaxi.mycoins

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import com.mytaxi.mycoins.adapter.MainCoins
import com.mytaxi.mycoins.adapter.MainCoinsAdapter
import com.mytaxi.mycoins.custom.MyDividerItemDecoration
import com.mytaxi.mycoins.custom.RecyclerItemClickListener
import com.mytaxi.mycoins.models.map.Data
import com.mytaxi.mycoins.models.map.Map
import com.mytaxi.mycoins.retrofit.RetrofitBuilder
import com.mytaxi.mycoins.service.CryptocurrencyService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    @BindView(R.id.mainRecyclerView)
    lateinit var recyclerView: RecyclerView

    private val coinsList: MutableList<MainCoins> = ArrayList()

    private lateinit var mAdapter: MainCoinsAdapter

    private var jsonElements: List<Data>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        addRecyclerClickListener()
        getMapData()
    }

    private fun addRecyclerClickListener() {
        recyclerView.addOnItemTouchListener(RecyclerItemClickListener(applicationContext, recyclerView, object : RecyclerItemClickListener.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                val intent = Intent(this@MainActivity, ListingActivity::class.java)
                intent.putExtra("id", jsonElements!![position].id)
                startActivity(intent)
            }

            override fun onLongItemClick(view: View?, position: Int) {
                // TODO
            }
        }))
    }

    private fun getMapData() {
        if (jsonElements != null) {
            loadRecyclerView()
        } else {
            val cryptocurrencyService: CryptocurrencyService = RetrofitBuilder.retrofitBuilder.create(CryptocurrencyService::class.java)
            val callMap: Call<Map> = cryptocurrencyService.mapCall()
            callMap.enqueue(object : Callback<Map> {
                override fun onResponse(call: Call<Map>, response: Response<Map>) {
                    if (response.code() == 200) {
                        val mapObject: Map? = response.body()
                        jsonElements = mapObject?.data
                        loadRecyclerView()
                    } else {
                        Log.d("debug", response.message())
                        Toast.makeText(applicationContext, R.string.toast_message, Toast.LENGTH_SHORT).show()
                    }

                }

                override fun onFailure(call: Call<Map>, t: Throwable) {
                    Log.d("debug", t.toString())
                    Toast.makeText(applicationContext, R.string.toast_message, Toast.LENGTH_SHORT).show()
                }
            })
        }
    }

    private fun loadRecyclerView() {
        mAdapter = MainCoinsAdapter(coinsList)
        val mLayoutManager = LinearLayoutManager(applicationContext)
        recyclerView.layoutManager = mLayoutManager
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.adapter = mAdapter
        recyclerView.addItemDecoration(MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16))
        for (x in 0 until jsonElements!!.size) {
            coinsList.add(MainCoins(jsonElements!![x].name,jsonElements!![x].symbol))
        }
        mAdapter.notifyDataSetChanged()
    }

}
