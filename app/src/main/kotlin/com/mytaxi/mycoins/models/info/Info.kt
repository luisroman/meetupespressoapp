package com.mytaxi.mycoins.models.info

data class Urls(
    val website: List<Any>,
    val twitter: List<Any>,
    val reddit: List<Any>,
    val message_board: List<Any>,
    val announcement: List<Any>,
    val chat: List<Any>,
    val explorer: List<Any>,
    val source_code: List<Any>
)

data class X10(
    val urls: Urls,
    val logo: String,
    val id: Int,
    val name: String,
    val symbol: String,
    val slug: String,
    val date_added: String,
    val tags: List<String>,
    val category: String
)
